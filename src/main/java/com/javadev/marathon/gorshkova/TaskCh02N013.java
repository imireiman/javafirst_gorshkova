package com.javadev.marathon.gorshkova;

import java.util.Scanner;

public class TaskCh02N013 {
    public static void main (String[] args){
        System.out.println("Введите 3-х значное число: ");
        Scanner in = new Scanner(System.in);

        int x = in.nextInt();
        int m = 0;

        for (int i = 0; i < 3; i++){
            m *= 10;
            m += x % 10;
            x /= 10;
        }
        System.out.println(m);
    }
}

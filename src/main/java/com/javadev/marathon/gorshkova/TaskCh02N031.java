package com.javadev.marathon.gorshkova;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main (String[] args){
        System.out.println("Введите число: ");
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        int x2 = n % 10;
        int x3 = (n % 100) / 10;
        int x1 = n / 100;
        int x = x1 *100 + x2 * 10 + x3;

        System.out.println(x);
    }
}

package com.javadev.marathon.gorshkova;

import java.util.Scanner;

public class TaskCh03N029 {
    public static void main(String args[]){
        Scanner input = new Scanner(System.in);
        System.out.println("Введите значение  x: ");
        int x = input.nextInt();
        System.out.println("Введите значение  y: ");
        int y = input.nextInt();
        System.out.println("Введите значение  z: ");
        int z = input.nextInt();
        /**
         * а) каждое из чисел X и Y нечетное;
         */
        final boolean b = x % 2 != 0 && y % 2 != 0;
        /**
         * б) только одно из чисел X и Y меньше 20;
         */
        final boolean c = (x < 20 && y >= 20) || (x >= 20 && y < 20);
        /**
         * в) хотя бы одно из чисел X и Y равно нулю;
         */
        final boolean d = (x == 0 || y == 0);
        /**
         * г) каждое из чисел X, Y, Z отрицательное;
         */
        final boolean e = (x < 0 && y < 0 && z < 0);
        /**
         * д) только одно из чисел X, Y и Z кратно пяти;
         */
        final boolean f = (x % 5 != 0 && y % 5 != 0 && z % 5 == 0) ||
                (x % 5 != 0 && y % 5 == 0 && z % 5 != 0) ||
                (x % 5 == 0 && y % 5 != 0 && z % 5 != 0);
        /**
         * е) хотя бы одно из чисел X, Y, Z больше 100.
         */
        final boolean s = (x > 100 || y > 100 || z > 100);
    }
}

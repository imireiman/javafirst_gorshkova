package com.javadev.marathon.gorshkova;

import java.util.Scanner;

public class TaskCh01N017 {

    public static void main (String[] args){
        System.out.println("Введите значение х: ");
        Scanner in = new Scanner(System.in);

        int x = in.nextInt();
        System.out.println("Введите значение a: ");
        int a = in.nextInt();
        System.out.println("Введите значение b: ");
        int b = in.nextInt();
        System.out.println("Введите значение c: ");
        int c = in.nextInt();

        double rez = Math.sqrt(1 - Math.pow((Math.sin(x)), 2));
        System.out.println("Ответ по первой формуле: " + rez);

        double rez2 = 1 / (Math.sqrt(a*Math.pow(x, 2) + b*x + c));
        System.out.println("Ответ по второй формуле: " + rez2);

        double rez3 = (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
        System.out.println("Ответ по третей формуле: " + rez3);

        double rez4 = (Math.abs(x) + Math.abs(x + 1));
        System.out.println("Ответ по четвертой формуле: " + rez4);

    }

}
